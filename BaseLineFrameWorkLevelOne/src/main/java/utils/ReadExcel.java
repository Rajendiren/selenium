package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	static Object[][] data;
	public static Object[][] readData(String datasheet)
	{
		// TODO Auto-generated method stub
		try {
			XSSFWorkbook wbook = new XSSFWorkbook("./Data/TC001.xlsx");
			XSSFSheet sheet= wbook.getSheet("Sheet1");
			int rowCount = sheet.getLastRowNum();
			System.out.println("Row count"+rowCount);
			int colcount =sheet.getRow(0).getLastCellNum();
			System.out.println("Column count"+colcount);
			Object[][] data= new Object[rowCount][colcount];
			
			for (int i = 1; i <=rowCount; i++) {
				XSSFRow row =sheet.getRow(i); 
				for (int j = 0; j < colcount; j++) {
					XSSFCell cell=row.getCell(j);
					data[i-j][j]=cell.getStringCellValue();
					String text=cell.getStringCellValue();
					System.out.println(text);
				}
				wbook.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
				
	}
}
