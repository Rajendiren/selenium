package utils;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reports {
	ExtentHtmlReporter html;
	ExtentReports extent;
	ExtentTest test;
	@Test
	public void runReport() {
		html= new ExtentHtmlReporter("./report/extentReport.html");
		extent = new ExtentReports();
		html.setAppendExisting(true);
		extent.attachReporter(html);
		test= extent.createTest("TC001_Login","Login into Leaftaps");
		test.assignAuthor("Ramya");
		test.assignCategory("smoke");
		try {
			test.fail("UserName DemoSalesManager entered successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		}
			catch(Exception e) {
		
			e.printStackTrace();
		}
		extent.flush();
		System.out.println("browser run successfullys");
	}

	
	
	
	
	
	}
