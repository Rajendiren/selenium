package bl.framework.testcases;

	import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

	import bi.framework.design.ProjectMethod;

	public class TC007_Create_Leads extends ProjectMethod{
		@Test(dataProvider="getData")
		public void createLeads(String Cname, String Fname, String Lname ) {
			WebElement eleCreateLead = locateElement("Linktext", "Create Lead");
			
			click(eleCreateLead);
			
			WebElement elecompanyName = locateElement("id","createLeadForm_companyName");
			
			clearAndType(elecompanyName,Cname);		
			
			WebElement elefirstName = locateElement("id", "createLeadForm_firstName");
			
			clearAndType(elefirstName, Fname);
			
			WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
			
			clearAndType(eleLastName, Lname);
			
			WebElement eleSubmit = locateElement("name","submitButton");
			
			click(eleSubmit);
			
		}
		@DataProvider(name="getData")
		public String[][] fetchdata()
		{
			String[][] data= new String[2][3];
			data[0][0]="Testleaf";
			data[0][1]="balaji";
			data[0][2]="c";
			data[1][0]="Testleaf";
			data[1][1]="koushik";
			data[1][2]="d";
			return data;
			
		}
		@DataProvider(name="getData1")
		public String[][] fetchdata1()
		{
			String[][] data= new String[2][3];
			data[0][0]="Testleaf";
			data[0][1]="balaji";
			data[0][2]="c";
			data[0][0]="Testleaf";
			data[0][1]="koushik";
			data[0][2]="d";
			return data;
			
		}
		public void click(WebElement eleCreateLead) {
			// TODO Auto-generated method stub
			
		}

		public WebElement locateElement(String string, String string2) {
			// TODO Auto-generated method stub
			return null;
		}

		public void clearAndType(WebElement eleLastName, String string) {
			// TODO Auto-generated method stub
			
		}

	}

