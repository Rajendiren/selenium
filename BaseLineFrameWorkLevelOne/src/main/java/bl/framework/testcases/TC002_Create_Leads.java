package bl.framework.testcases;

	import org.openqa.selenium.WebElement;
	import org.testng.annotations.Test;

	import bi.framework.design.ProjectMethod;

	public class TC002_Create_Leads extends ProjectMethod{
		@Test(groups= {"smoke"})
		//(invocationCount=2,threadPoolSize=2)
		public void createLeads() {
			
			
			
			WebElement eleCreateLead = locateElement("Linktext", "Create Lead");
			
			click(eleCreateLead);
			
			WebElement elecompanyName = locateElement("id","createLeadForm_companyName");
			
			clearAndType(elecompanyName, "Infosys");		
			
			WebElement elefirstName = locateElement("id", "createLeadForm_firstName");
			
			clearAndType(elefirstName,"Praveena");
			
			WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
			
			clearAndType(eleLastName, "Mohandass");
			
			WebElement eleSubmit = locateElement("name","submitButton");
			
			click(eleSubmit);
			
		}

		public void click(WebElement eleCreateLead) {
			// TODO Auto-generated method stub
			
		}

		public WebElement locateElement(String string, String string2) {
			// TODO Auto-generated method stub
			return null;
		}

		public void clearAndType(WebElement eleLastName, String string) {
			// TODO Auto-generated method stub
			
		}

	}

