package bl.framework.testcases;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;


public class TC_004_EditLeads extends SeleniumBase {
    @Test
	public void editLeads() throws InterruptedException{
	startApp("chrome", "http://leaftaps.com/opentaps");
	WebElement eleUsername = locateElement("id", "username");
	clearAndType(eleUsername, "DemoSalesManager"); 
	WebElement elePassword = locateElement("id", "password");
	clearAndType(elePassword, "crmsfa"); 
	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	click(eleLogin);
    WebElement eleLink = locateElement("Linktext","CRM/SFA");
	click(eleLink);
	WebElement leads = locateElement("Linktext", "Leads");
	click(leads);
	WebElement findLeads = locateElement("xpath", "//a[@href='/crmsfa/control/findLeads']");
	click(findLeads);
	WebElement firstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
	clearAndType(firstName, "Gopi");
	WebElement findLead= locateElement("xpath", "//button[text()='Find Leads']");
	click(findLead);
	Thread.sleep(3000);
	WebElement ele= driver.findElementByXPath("//table[@class ='x-grid3-row-table']");
	
	List<WebElement> elerow = ele.findElements(By.tagName("tr"));
	
	WebElement eleCols= locateElement("tagname", "td");
	
	eleCols.findElement(By.xpath("(//a[@class='linktext'])[4]"));
	
	
	
	
	String title = locateElement("xpath","//div[text()='View Lead']").getText();
	if(verifyTitle(title)==true) {
		System.out.println("The title is verified as View lead");
	}
	else {
		System.out.println("The title is not verified as View lead");
	}
WebElement edit = locateElement("xpath", "//a[text()='Edit']");
	
	click(edit);
	
	WebElement companyName = locateElement("id", "updateLeadForm_companyName");
	clearAndType(companyName, "Infy");
	
	String str = companyName.getText();
	WebElement update = locateElement("xpath", "//input[@value='Update']");
	
	String str1 = locateElement("xpath", " //span[@id='viewLead_companyName_sp']").getText();	

	if (str.contains(str1)) {
		System.out.println("The company name is updated");
	}
//Close the browser
	
	driver.close();
	
}
}