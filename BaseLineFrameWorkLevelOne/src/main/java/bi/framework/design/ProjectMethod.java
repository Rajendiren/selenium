package bi.framework.design;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;

import bl.framework.api.SeleniumBase;
import utils.ReadExcel;

public class ProjectMethod extends SeleniumBase {
	public String dataSheetname;
	public void login()
	{
		//@Test
		startApp("chrome","http://leaftaps.com/opentaps");
		WebElement eleUserName= locateElement("id","username");
		clearAndType(eleUserName,"DemoSalesangaer");
		WebElement elePassword=locateElement("id","Password");
		clearAndType(elePassword, "crmsfa");
		WebElement eleLogin=locateElement("class","decorativeSubmit");	
	}
	@DataProvider(name="fetchdata")
	public Object[][] getData()
	
	{
		return ReadExcel.readData(dataSheetname);
		
	}
			

}
